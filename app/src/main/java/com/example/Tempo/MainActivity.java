package com.example.Tempo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {


    private TextView tvClimaChillan;
    private TextView tvHumedadChillan;
    private TextView tvPresionChillan;

    private TextView tvClimaMoscu;
    private TextView tvHumedadMoscu;
    private TextView tvPresionMoscu;

    private TextView tvClimaMadrid;
    private TextView tvHumedadMadrid;
    private TextView tvPresionMadrid;

    private TextView tvClimaParis;
    private TextView tvHumedadParis;
    private TextView tvPresionParis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.tvClimaChillan = (TextView) findViewById(R.id.tvClimaChillan);
        this.tvClimaParis = (TextView) findViewById(R.id.tvClimaParis);
        this.tvClimaMadrid = (TextView) findViewById(R.id.tvClimaMadrid);
        this.tvClimaMoscu = (TextView) findViewById(R.id.tvClimaMoscu);
        this.tvPresionParis = (TextView) findViewById(R.id.tvPresionParis);
        this.tvPresionMadrid = (TextView) findViewById(R.id.tvPresionMadrid);
        this.tvPresionMoscu = (TextView) findViewById(R.id.tvPresionMoscu);
        this.tvPresionChillan = (TextView) findViewById(R.id.tvPresionChillan);
        this.tvHumedadChillan = (TextView) findViewById(R.id.tvHumedadChillan);
        this.tvHumedadMadrid = (TextView) findViewById(R.id.tvHumedadMadrid);
        this.tvHumedadMoscu = (TextView) findViewById(R.id.tvHumedadMoscu);
        this.tvHumedadParis = (TextView) findViewById(R.id.tvHumedadParis);

        String urlChillan = "http://api.openweathermap.org/data/2.5/weather?lat=-36.6067&lon=-72.1034&appid=85eae427e5c3da015493eab59478044c&units=metric";


        StringRequest soldatos = new StringRequest(
                Request.Method.GET,
                urlChillan,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                            double temp = mainJSON.getDouble("temp");
                            double humedad = mainJSON.getDouble("humidity");
                            double presion = mainJSON.getDouble("pressure");
                            tvClimaChillan.setText("Temperatura = " + temp + "°C");
                            tvHumedadChillan.setText("Humedad = " + humedad + "%" );
                            tvPresionChillan.setText("Presión = " + presion +" Ps");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }
        );
        String urlMoscu = "http://api.openweathermap.org/data/2.5/weather?lat=55.7522202&lon=37.6155586&appid=85eae427e5c3da015493eab59478044c&units=metric";
        StringRequest soldatos2 = new StringRequest(
                Request.Method.GET,
                urlMoscu,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                            double temp = mainJSON.getDouble("temp");
                            double humedad = mainJSON.getDouble("humidity");
                            double presion = mainJSON.getDouble("pressure");
                            tvClimaMoscu.setText("Temperatura = " + temp + "°C");
                            tvHumedadMoscu.setText("Humedad = " + humedad + "%" );
                            tvPresionMoscu.setText("Presión = " + presion +" Ps" );

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo falla
                    }
                }
        );
        String urlMadrid = "http://api.openweathermap.org/data/2.5/weather?lat=40.4167&lon=-3.70325&appid=85eae427e5c3da015493eab59478044c&units=metric";
        StringRequest soldatos3 = new StringRequest(
                Request.Method.GET,
                urlMadrid,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                            double temp = mainJSON.getDouble("temp");
                            double humedad = mainJSON.getDouble("humidity");
                            double presion = mainJSON.getDouble("pressure");
                            tvClimaMadrid.setText("Temperatura = " + temp + "°C");
                            tvHumedadMadrid.setText("Humedad = " + humedad + "%" );
                            tvPresionMadrid.setText("Presión = " + presion + " Ps");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo falla
                    }
                }
        );
        String urlParis = "http://api.openweathermap.org/data/2.5/weather?lat=48.8667&lon=2.333335&appid=85eae427e5c3da015493eab59478044c&units=metric";
        StringRequest soldatos4 = new StringRequest(
                Request.Method.GET,
                urlParis,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject respuestaJSON = new JSONObject(response);
                            JSONObject mainJSON = respuestaJSON.getJSONObject("main");
                            double temp = mainJSON.getDouble("temp");
                            double humedad = mainJSON.getDouble("humidity");
                            double presion = mainJSON.getDouble("pressure");
                            tvClimaParis.setText("Temperatura = " + temp + "°C");
                            tvHumedadParis.setText("Humedad = " + humedad + "%" );
                            tvPresionParis.setText("Presión = " + presion + " Ps");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo Falla
                    }
                }
        );

        RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
        listaEspera.add(soldatos);
        RequestQueue listaEspera2 = Volley.newRequestQueue(getApplicationContext());
        listaEspera2.add(soldatos2);
        RequestQueue listaEspera3 = Volley.newRequestQueue(getApplicationContext());
        listaEspera3.add(soldatos3);
        RequestQueue listaEspera4 = Volley.newRequestQueue(getApplicationContext());
        listaEspera4.add(soldatos4);
    }
}
